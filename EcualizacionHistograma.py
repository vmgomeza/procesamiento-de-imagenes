import cv2 as cv
from matplotlib import pyplot as plt


#img = plt.imread("./Imagenes/fondo.jpg")
img = cv.imread("./Imagenes/fondo.jpg")
img_to_yuv = cv.cvtColor(img, cv.COLOR_BGR2YUV)
img_to_yuv[:, :, 0] = cv.equalizeHist(img_to_yuv[:, :, 0])
hist_equalization_result = cv.cvtColor(img_to_yuv, cv.COLOR_YUV2BGR)
hist_original = cv.calcHist([img],[0],None,[256],[0,256])
hist_ecualizada = cv.calcHist([hist_equalization_result],[0],None,[256],[0,256])
plt.subplot(221), plt.imshow(img)
plt.subplot(222), plt.plot(hist_original)
plt.subplot(223), plt.imshow(hist_equalization_result)
plt.subplot(224), plt.plot(hist_ecualizada)
plt.xlim([0,256])
plt.show()


