# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Valeria Gómez - 21 diciembre 2020
# Operaciones Logicas
import cv2 as cv

# Leer imagen Simón Bolívar
imagen1 = cv.imread("./cuadrado2.png")
# Leer imagen Torre Eiffel
imagen2 = cv.imread("./circulo2.png")
# Realizar operaciones solicitadas
#Indica el tamaño de la imagen
print(imagen1.shape)
print(imagen2.shape)
#Re dimensionar la imagen
imagen1_res = cv.resize(imagen1, dsize=(400, 350), interpolation=cv.INTER_CUBIC)
imagen2_res = cv.resize(imagen2, dsize=(400, 350), interpolation=cv.INTER_CUBIC)

#Operaciones logicas con Bitwise (Combina los pixeles correspondientes de cada imagen)
#Operacion Logica: AND (Verdadero si ambos pixeles son mayores que cero)
bitwise_and = cv.bitwise_and(imagen1_res, imagen2_res)

#Operacion Logica: OR (Verdadero si uno de los pixeles es mayor que cero)
bitwise_or = cv.bitwise_or(imagen1_res,imagen2_res)

#Operacion Logica: XOR (Verdadero si uno de los pixeles es mayor que cero, pero no los dos al mismo tiempo)
bitwise_xor = cv.bitwise_xor(imagen1_res,imagen2_res)

#Operacion Logica: NOT (Negacion: Si es o pasa a ser 1, y si es 1 pasa a ser 0)
bitwise_not = cv.bitwise_not(imagen1_res)

#Mostrar imagenes
#cv.imshow('Imagen 1',imagen1_res)
#cv.imshow('Imagen 2',imagen2_res)
cv.imshow('Operacion Logica AND',bitwise_and)
cv.imshow('Operacion Logica OR',bitwise_or)
cv.imshow('Operacion Logica XOR',bitwise_xor)
cv.imshow('Operacion Logica NOT',bitwise_not)

cv.waitKey(0)
cv.destroyAllWindows()