# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# Valeria Gómez - 21 diciembre 2020
# Operaciones Aritmeticas
import cv2 as cv
# Leer imagen Simón Bolívar
imagen1 = cv.imread("./cuadrado2.png")
# Leer imagen Torre Eiffel
imagen2 = cv.imread("./circulo2.png")
# Realizar operaciones solicitadas
#Indica el tamaño de la imagen
print(imagen1.shape)
print(imagen2.shape)
#Re dimensionar la imagen
imagen1_res = cv.resize(imagen1, dsize=(512, 512), interpolation=cv.INTER_CUBIC)
imagen2_res = cv.resize(imagen2, dsize=(512, 512), interpolation=cv.INTER_CUBIC)

# Usar OpenCV para Adición de imágenes add:suma las imagenes  addWeighted:suma las imagenes dando peso
#sumaimg = cv.add(imagen1_res,imagen2)
#sumaimg = cv.addWeighted(imagen1_res,0.8,imagen2,0.2,0)
# Usar OpenCV para Sustracción  de imágenes
#restaimg =cv.subtract(imagen2,imagen1_res)
abs = cv.absdiff(imagen1_res,imagen2_res)

#Mostrar imagenes
cv.imshow('Imagen 1',imagen1)
cv.imshow('Imagen 2',imagen2)
cv.imshow('Suma de imagenes',abs)

cv.waitKey(0)
cv.destroyAllWindows()