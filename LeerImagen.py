import cv2 as cv
import numpy
#Leer imagen con OpenCV
#Leer imagen interna
imagen = cv.imread("./Imagenes/Lena.png")
#Separar canales de la imagen
blue,green, red = cv.split(imagen)
#Crear vector lleno de ceros para mostrar la imagen separada coloreada
zeros = numpy.zeros(blue.shape, numpy.uint8)
#Unimos los vectores de los canales
blueBGR = cv.merge((blue,zeros,zeros))
greenBGR = cv.merge((zeros,green,zeros))
redBGR = cv.merge((zeros,zeros,red))
#Mostramos los canales
cv.imshow('blue BGR', blueBGR)
cv.imshow('green BGR', greenBGR)
cv.imshow('red BGR', redBGR)
#Leer imagen externa
#imagen = cv.imread("../Externo/fcb.jpg")
#Transformar imagen de color a escala de grises
gray = cv.cvtColor(imagen,cv.COLOR_BGR2GRAY)
#Convertir a imagen binaria (blanco&negro)
(thresh, blackAndWhite) = cv.threshold(gray, 68, 255, cv.THRESH_BINARY)
#Muestra pantalla con un titulo
cv.imshow('Lena',imagen)
cv.imshow('Lena en Gris',gray)
cv.imshow('Lena en blanco y negro',blackAndWhite)
#Guardar la imagen resultante en el mismo directorio
#cv.imwrite('Lena_en_Gris.png',gray)
#cv.imwrite('Lena_en_Binario.png',blackAndWhite)
#Guardar la imagen resultante en un directorio externo
cv.imwrite('./Salida/Lena_en_Gris2.png',gray)
cv.imwrite('./Salida/Lena_en_Binario2.png',blackAndWhite)
#Presiona una tecla para q la ventana sea destruida
cv.waitKey(0)
#Cierra la ventana
cv.destroyAllWindows() 